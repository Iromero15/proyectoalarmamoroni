
#include <Arduino.h>
#if defined(ESP32)
  #include <WiFi.h>
#elif defined(ESP8266)
  #include <ESP8266WiFi.h>
#endif
#include <Firebase_ESP_Client.h>

//Provide the token generation process info.
#include "addons/TokenHelper.h"
//Provide the RTDB payload printing info and other helper functions.
#include "addons/RTDBHelper.h"

// Insert your network credentials
#define WIFI_SSID "Red de nachito"
#define WIFI_PASSWORD "capoelqueseconecta"

// Insert Firebase project API Key
#define API_KEY "AIzaSyBILboqeNqvQ2YBo0dKBmqNsSyDzbkp82I"

// Insert RTDB URLefine the RTDB URL */
#define DATABASE_URL "https://alarma-etrr-esp-32-default-rtdb.firebaseio.com/" 

// UID and other stuff
const String UID = "/q1eyphHVibTLPyB6d8KRxWtbS192";
const String alarmStatus = "/ESTADO_DE_ALARMA";
const String functStatus = "/ESTADO_DE_FUNCIONAMIENTO";

// Define Pin Number used
#define BUZZER 5
#define PIR 21
#define LED 18
//Define Firebase Data object

FirebaseData fbdo;
FirebaseAuth auth;
FirebaseConfig config;

unsigned long sendDataPrevMillis = 0;
bool alarmState = false;
bool signupOK = false; 

void setup() {

  Serial.begin(115200); //Velocidad de conexión mediante cable

  pinMode(LED, OUTPUT);      // declare LED as output
  pinMode(PIR, INPUT);     // declare sensor as input
  pinMode(BUZZER, OUTPUT);
  WiFi.begin(WIFI_SSID, WIFI_PASSWORD); // Conexion basica de WI-FI

  while (WiFi.status() != WL_CONNECTED){
    Serial.print(".");
    delay(300);
    delay(300);
  }

  Serial.println();
  Serial.print("Connected with IP: ");
  Serial.println(WiFi.localIP());
  Serial.println();

  config.api_key = API_KEY;
  config.database_url = DATABASE_URL;
    
  if (Firebase.signUp(&config, &auth, "","")){
    Serial.println("ok");
    signupOK = true;
  }
  else{
    Serial.println("Error");
    Serial.printf("%s\n", config.signer.signupError.message.c_str());
  }

  config.token_status_callback = tokenStatusCallback;

  Firebase.begin(&config, &auth);

  Firebase.reconnectWiFi(true);
}

void ActivateState()
{

  String Aux = "/Usuario";
  Aux.concat(UID);
  Aux.concat(alarmStatus);

  Serial.println("ActivateState");


  if ((Firebase.RTDB.setBool(&fbdo, Aux, true))){
      Serial.println("PASSED");
      digitalWrite(5, HIGH);
    } 
    else {
      Serial.println("FAILED");
    }
}

bool ReadState()
{
    String Aux = "/Usuario";
    Aux.concat(UID);
    Aux.concat(alarmStatus);

    Serial.println("ReadState");

  if (Firebase.RTDB.getBool(&fbdo, Aux)) {
      
      if (fbdo.dataType() == "boolean") {
        Serial.println(fbdo.boolData());
        return fbdo.boolData();    
      }
    }
    else {
      Serial.println("Error");
      Serial.println(fbdo.errorReason());
    }
}

bool ReadFunct()
{
  String Aux = "/Usuario";
  Aux.concat(UID);
  Aux.concat(functStatus);

   Serial.println("ReadFunct");

  if (Firebase.RTDB.getBool(&fbdo, Aux)) {
      if (fbdo.dataType() == "boolean") {
        Serial.println(fbdo.boolData());
        return fbdo.boolData();
      }
    }
    else {
      Serial.println("Error");
      Serial.println(fbdo.errorReason());
      return false;
    }
}


void loop(){
  if (Firebase.ready() && signupOK && (millis() - sendDataPrevMillis > 100 || sendDataPrevMillis == 0)){
    
    sendDataPrevMillis = millis();
    // Write an BOOL number on the database path
    digitalWrite(LED, ReadFunct());
    if((digitalRead(PIR)) && (ReadFunct()))
    {
      ActivateState();
      delay(500);
    }
    if(!ReadState())
    {
      Serial.println("Low");
      digitalWrite(BUZZER, LOW);
    }
  }
}